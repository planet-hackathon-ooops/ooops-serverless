import os
import sys

import requests

from src.logger import get_logger
from src.response import response_client_error, response_ok, response_server_error


def parse_qs(qs):
    return qs.get('cityCode'), qs.get('page', 1)


class AptlistAPI(object):

    def __init__(self, query_string):
        self.qs = query_string
        self.logger = get_logger(__name__)
        self.service_name = self.qs.get('service')
        self.api_client = self.client_session

    def fetch(self) -> requests.Response:
        self.logger.info(f'{self.service_name} API called with {self.api_endpoint}')
        self.logger.info(f'Parameters: {self.requested_params}')
        return self.api_client.get(self.api_endpoint, params=self.requested_params)

    @property
    def client_session(self):
        session = requests.Session()
        session.headers.update({
            'Content-Type': 'application/json;charset=utf8'
        })
        return session

    @property
    def api_endpoint(self):
        return f'{os.environ.get("WASTE_API_BASE_URL")}/getAptlist'

    @property
    def requested_params(self):
        cityCode, page = parse_qs(self.qs)

        return {
            "ServiceKey": os.environ.get('ServiceKey'),
            "type": "json",
            "cityCode": cityCode,
            "page": page,
            "rowNum": 10
        }


def main(event, context):
    if not event['queryStringParameters']:
        return response_client_error(None)

    apt_api = AptlistAPI(event['queryStringParameters'])
    try:
        response = apt_api.fetch()
        if response.ok:
            apt_api.logger.info(f'Apt list successfully fetched: {response.status_code}')
            return response_ok(response.json())
        else:
            return response_server_error(None, response.status_code)
    except Exception as e:
        _, _, trace_back = sys.exc_info()
        apt_api.logger.error(e, exc_info=True)
        apt_api.logger.error(trace_back)
