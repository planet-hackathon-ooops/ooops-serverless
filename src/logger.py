import logging
import os


def get_logger(module_name):
    logger = logging.getLogger(module_name)
    log_level = os.environ.get('currentStage') == 'dev' and logging.DEBUG or logging.INFO
    logger.setLevel(log_level)
    return logger
