import sys

import requests

from src.logger import get_logger
from src.response import response_client_error, response_ok


def parse_qs(qs):
    return qs.get('year'), qs.get('month'), qs.get('day'), qs.get('cityCode')


class WasteAvgAPI(object):

    def __init__(self, query_string):
        self.qs = query_string
        self.logger = get_logger(__name__)
        self.api_client = self.client_session

    def fetch(self) -> requests.Response:
        self.logger.info(f'WasteAvgAPI API called with {self.api_endpoint}')
        self.logger.info(f'Parameters: {self.requested_params}')

        response = self.api_client.get(self.api_endpoint, params=self.requested_params).json()
        self.logger.info(f'### Cites Wastes: {response}')

        totalWaste = 0
        for cityObj in response:
            self.logger.info(f'### cityObj: {cityObj}')
            totalWaste += cityObj['wasteTotal']

        return {
                    'avg' : totalWaste / len(response) if (len(response) > 0) else 0, 
                    'total' : totalWaste
                }

    @property
    def client_session(self):
        session = requests.Session()
        session.headers.update({
            'Content-Type': 'application/json;charset=utf8'
        })
        return session

    @property
    def api_endpoint(self):
        cityCode = self.qs.get('cityCode')
        if not cityCode:
            return f'https://ovbvbxecc0.execute-api.ap-northeast-2.amazonaws.com/dev/ooops/waste/cities/'
        else:
            return f'https://ovbvbxecc0.execute-api.ap-northeast-2.amazonaws.com/dev/ooops/waste/{cityCode}/'

    @property
    def requested_params(self):
        year, month, day, cityCode = parse_qs(self.qs)

        return {
            "year": year,
            "month": month,
            "day": day,
            "cityCode": cityCode
        }


def main(event, context):
    if not event['queryStringParameters']:
        return response_client_error(None)

    api = WasteAvgAPI(event['queryStringParameters'])
    try:
        response = api.fetch()
        api.logger.info(f'Average successfully calculated: {response}')
        return response_ok(response)

    except Exception as e:
        _, _, trace_back = sys.exc_info()
        api.logger.error(e, exc_info=True)
        api.logger.error(trace_back)
