import json


def get_default_response_body(status_code, body):
    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True
        },
        "body": json.dumps(body)
    }


def response_ok(body_value, status_code=200):
    return get_default_response_body(status_code, body_value)


def response_server_error(body_value, status_code=500):
    return get_default_response_body(status_code, body_value)


def response_client_error(body_value, status_code=400):
    return get_default_response_body(status_code, body_value)
