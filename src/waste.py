import abc
import json
import os
import sys

import requests

from src.decorators import validate_query_string
from src.logger import get_logger
from src.response import response_ok, response_server_error


def parse_qs(qs):
    return qs.get('year'), qs.get('month'), qs.get('day'), qs.get('cityCode')


class WasteAPI(metaclass=abc.ABCMeta):

    def __init__(self, qs, path):
        self.qs = qs
        self.path = path
        self.year = self.qs.get('year')
        self.month = self.qs.get('month')
        self.day = self.qs.get('day')
        self.logger = get_logger(__name__)
        self.api_client = self.client_session

    def fetch(self, **kwargs) -> requests.Response:
        params = self.get_request_param(kwargs)
        self.logger.info(f'{self.service} API called with {self.api_endpoint}')
        self.logger.info(f'Parameters: {params}')
        return self.api_client.get(self.api_endpoint, params=params)

    @property
    @abc.abstractmethod
    def service(self):
        raise NotImplementedError

    @property
    def api_endpoint(self):
        return os.environ.get("WASTE_API_BASE_URL") + os.environ.get(self.service)

    def get_request_param(self, update_value):
        request_param = self.default_request_param
        request_param.update(update_value)
        return request_param

    @property
    def client_session(self):
        session = requests.Session()
        session.headers.update({
            'Content-Type': 'application/json;charset=utf8'
        })
        return session

    @property
    @abc.abstractmethod
    def row_num(self):
        raise NotImplementedError

    @property
    def default_request_param(self):
        return {
            "ServiceKey": os.environ.get('ServiceKey'),
            "type": "json",
            "page": 1,
            "rowNum": self.row_num
        }

    def get_city_code(self) -> requests.Response:
        ep = 'https://ovbvbxecc0.execute-api.ap-northeast-2.amazonaws.com/dev/ooops/cityList'
        params = {
            'citySidoName': '서울',
            'service': 'cityList'
        }
        return self.client_session.get(ep, params=params)

    @abc.abstractmethod
    def get_daily_data(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_monthly_data(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_yearly_data(self):
        raise NotImplementedError

    def get_calc_method(self):
        if self.day:
            return self.get_daily_data
        if not self.day and self.month:
            return self.get_monthly_data
        if not self.day and not self.month and self.year:
            return self.get_yearly_data


class CitiesWasteAPI(WasteAPI):

    def __init__(self, qs, path):
        super().__init__(qs, path)

    def get_daily_data(self):
        cities = self.get_city_code().json()
        results = []
        for city in cities['data']['list']:
            result = self.fetch(cityCode=city['cityCode'], disMonth=self.month, disYear=self.year)
            try:
                data_list = result.json()
                if data_list['data']['resultCode'] != 'OK':
                    continue
            except json.decoder.JSONDecodeError as e:
                self.logger.warn(f'City Skipped: {city["cityCode"]}')
                self.logger.error(e)
                continue

            for dailyData in data_list['data']['list']:
                self.logger.info(dailyData)
                if dailyData['disDate'] != self.day:
                    continue
                results.append({
                    'cityName': dailyData['citySggName'],
                    'wasteTotal': dailyData['disQuantity'],
                    'wasteCount': dailyData['disCount']
                })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    def get_monthly_data(self):
        cities = self.get_city_code().json()
        results = []
        for city in cities['data']['list']:
            result = self.fetch(cityCode=city['cityCode'], disMonth=self.month, disYear=self.year)
            try:
                data_list = result.json()
                if data_list['data']['resultCode'] != 'OK':
                    continue
            except json.decoder.JSONDecodeError as e:
                self.logger.warn(f'City Skipped: {city["cityCode"]}')
                self.logger.error(e)
                continue

            total_amount = 0
            total_count = 0
            for city in result.json()['data']['list']:
                total_amount += city['disQuantity']
                total_count += city['disCount']
            results.append({
                'cityName': city['citySggName'],
                'wasteTotal': total_amount,
                'wasteCount': total_count
            })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    def get_yearly_data(self):
        cities = self.get_city_code().json()
        results = []

        for city in cities['data']['list']:
            inner = []
            outer_amount = 0
            outer_count = 0
            for i in range(1, 13):
                i = len(str(i)) < 2 and f'0{i}' or i
                result = self.fetch(cityCode=city['cityCode'], disMonth=i, disYear=self.year)
                try:
                    data_list = result.json()
                    if data_list['data']['resultCode'] != 'OK':
                        continue
                except json.decoder.JSONDecodeError as e:
                    self.logger.warn(f'City Skipped: {city["cityCode"]}')
                    self.logger.error(e)
                    continue

                total_amount = 0
                total_count = 0
                for city in result.json()['data']['list']:
                    total_amount += city['disQuantity']
                    total_count += city['disCount']
                inner.append({
                    'cityName': city['citySggName'],
                    'wasteTotal': total_amount,
                    'wasteCount': total_count
                })
            for v in inner:
                outer_amount += v['wasteTotal']
                outer_count += v['wasteCount']

            results.append({
                'cityName': v['cityName'],
                'wasteTotal': outer_amount,
                'wasteCount': outer_count
            })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    @property
    def row_num(self):
        return 31

    @property
    def service(self):
        return 'city_date'


class APTsWasteAPI(WasteAPI):

    def __init__(self, qs, path):
        super().__init__(qs, path)
        self.city_code = self.path.get('city')

    def get_apt_code(self):
        ep = 'https://ovbvbxecc0.execute-api.ap-northeast-2.amazonaws.com/dev/ooops/aptlist'
        params = {
            'cityCode': self.city_code,
            'service': 'aptlist'
        }
        return self.client_session.get(ep, params=params)

    def get_daily_data(self):
        apts = self.get_apt_code().json()
        results = []
        for apt in apts['data']['list']:
            result = self.fetch(cityCode=self.city_code, aptCode=apt['aptCode'], disMonth=self.month, disYear=self.year)
            try:
                data_list = result.json()
                if data_list['data']['resultCode'] != 'OK':
                    continue
            except json.decoder.JSONDecodeError as e:
                self.logger.warn(f'City Skipped: {apt["aptCode"]}')
                self.logger.error(e)
                continue

            for dailyData in data_list['data']['list']:
                if dailyData['disDate'] != self.day:
                    continue
                results.append({
                    'aptName': dailyData['aptName'],
                    'wasteTotal': dailyData['disQuantity'],
                    'wasteCount': dailyData['disCount']
                })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    def get_monthly_data(self):
        apts = self.get_apt_code().json()
        results = []
        for apt in apts['data']['list']:
            result = self.fetch(cityCode=self.city_code, aptCode=apt['aptCode'], disMonth=self.month,
                                disYear=self.year)
            try:
                data_list = result.json()
                if data_list['data']['resultCode'] != 'OK':
                    continue
            except json.decoder.JSONDecodeError as e:
                self.logger.warn(f'City Skipped: {apt["cityCode"]}')
                self.logger.error(e)
                continue

            total_amount = 0
            total_count = 0
            for daily in result.json()['data']['list']:
                total_amount += daily['disQuantity']
                total_count += daily['disCount']
            results.append({
                'aptName': daily['aptName'],
                'wasteTotal': total_amount,
                'wasteCount': total_count
            })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    def get_yearly_data(self):
        apts = self.get_apt_code().json()
        results = []

        for apt in apts['data']['list']:
            inner = []
            outer_amount = 0
            outer_count = 0
            for i in range(1, 13):
                i = len(str(i)) < 2 and f'0{i}' or i
                result = self.fetch(cityCode=self.city_code, aptCode=apt['aptCode'], disMonth=i, disYear=self.year)
                try:
                    data_list = result.json()
                    if data_list['data']['resultCode'] != 'OK':
                        continue
                except json.decoder.JSONDecodeError as e:
                    self.logger.warn(f'City Skipped: {apt["cityCode"]}')
                    self.logger.error(e)
                    continue

                total_amount = 0
                total_count = 0
                for daily in result.json()['data']['list']:
                    total_amount += daily['disQuantity']
                    total_count += daily['disCount']
                inner.append({
                    'aptName': daily['aptName'],
                    'wasteTotal': total_amount,
                    'wasteCount': total_count
                })

            for v in inner:
                outer_amount += v['wasteTotal']
                outer_count += v['wasteCount']

            results.append({
                'aptName': v['aptName'],
                'wasteTotal': outer_amount,
                'wasteCount': outer_count
            })

        ranked = [r for r in sorted(results, key=lambda x: x['wasteTotal'], reverse=True)]
        for i, rank in enumerate(ranked):
            rank['rank'] = i + 1
        return ranked

    @property
    def row_num(self):
        return 100

    @property
    def service(self):
        return 'apt_date'


@validate_query_string
def cities(qs, path, event, context):
    api = CitiesWasteAPI(qs, path)

    api.logger.info(f'Event Received: {json.dumps(event)}')
    api.logger.info(f'Context Received: {context.__dict__}')

    try:
        calc_method = api.get_calc_method()
        response = calc_method()
        if response:
            api.logger.info(f'Waste amount of city each dates successfully fetched: {len(response)}')
            return response_ok(response)
        else:
            return response_server_error(None, response)
    except Exception as e:
        _, _, trace_back = sys.exc_info()
        api.logger.error(e, exc_info=True)
        api.logger.error(trace_back)


@validate_query_string
def apts(qs, path, event, context):
    api = APTsWasteAPI(qs, path)

    api.logger.info(f'Event Received: {json.dumps(event)}')
    api.logger.info(f'Context Received: {context.__dict__}')

    try:
        calc_method = api.get_calc_method()
        response = calc_method()
        api.logger.info(response)
        if response:
            api.logger.info(f'Waste amount of city each dates successfully fetched: {len(response)}')
            return response_ok(response)
        else:
            return response_server_error(None, response)
    except Exception as e:
        _, _, trace_back = sys.exc_info()
        api.logger.error(e, exc_info=True)
        api.logger.error(trace_back)
