import os
import sys
import requests

from src.logger import get_logger
from src.response import response_client_error, response_ok, response_server_error


def parse_qs(qs):
    return qs.get('year'), qs.get('month'), qs.get('day'), qs.get('cityCode')


class WasteHistoryAPI(object):

    def __init__(self, query_string):
        self.qs = query_string
        self.logger = get_logger(__name__)
        self.api_client = self.client_session

    def fetch(self) -> requests.Response:
        self.logger.info(f'WasteHistoryAPI called with {self.api_endpoint}')
        self.logger.info(f'Parameters: {self.requested_params}')

        year, month, day, cityCode = parse_qs(self.qs)

        itrCnt = 0

        if (day != '') {
            itrCnt = 30
        } else if (month != '') {
            itrCnt = 12
        } else if (year != '') {
            itrCnt = 3
        }

        # for x in range(1, itrCnt):


        response = self.api_client.get(self.api_endpoint, params=self.requested_params)
        self.logger.info(f'### Cites Wastes History: {response}')

        totalWaste, citeCnt = 0
        for waste in response['']:
            totalWaste = float(totalWaste) + float(waste)
            citeCnt = citeCnt + 1

        return int(totalWaste / citeCnt) if (citeCnt > 0) else 0

    @property
    def client_session(self):
        session = requests.Session()
        session.headers.update({
            'Content-Type': 'application/json;charset=utf8'
        })
        return session

    @property
    def api_endpoint(self):
        cityCode = self.qs.get('cityCode')

        api_dscd = 'cites'
        if (cityCode != ''):
            api_dscd = 'apts'

        return f'https://yz42hnima2.execute-api.ap-northeast-2.amazonaws.com/dev/ooops/waste/{api_dscd}/'

    @property
    def requested_params(self):
        year, month, day, cityCode = parse_qs(self.qs)

        return {
            "year": year,
            "month": month,
            "day": day,
            "cityCode": cityCode
        }


def main(event, context):
    if not event['queryStringParameters']:
        return response_client_error(None)

    api = WasteHistoryAPI(event['queryStringParameters'])
    try:
        response = api.fetch()
        if response.ok:
            api.logger.info(f'History successfully calculated: {response.status_code}')
            return response_ok(response.json())
        else:
            return response_server_error(None, response.status_code)
    except Exception as e:
        _, _, trace_back = sys.exc_info()
        api.logger.error(e, exc_info=True)
        api.logger.error(trace_back)