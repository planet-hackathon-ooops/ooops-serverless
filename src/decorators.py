from src.response import response_client_error


def validate_query_string(fn):
    def wrapper(*args, **kwargs):
        event, context = args
        qs, path = event.get('queryStringParameters'), event.get('pathParameters')
        if not qs:
            return response_client_error(400, 'QueryString Required')
        return fn(qs, path, event, context)

    return wrapper
